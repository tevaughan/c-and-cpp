
\chapter{Introduction}

Each of C and C++ is a \emph{general-purpose} programming language.  C++
evolved from C and is mostly backward-compatible.  Almost every C program is a
valid C++ program.  Each is suitable for a wide range of applications, from
embedded systems to word-processors.

\section*{The Emergence and Spread of C}

\index{UNIX}\index{programming!structured}C emerged from the development of
UNIX in the early 1970s.  C~was designed to be a portable language for
structured programming, full use of the computer's resources, and static
typing, enabling the compiler to check for errors related to the use of
variables and functions.  C~eventually came to dominate the development of
operating systems.  The kernel of Microsoft Windows, the Linux kernel, and the
kernel of Mac~OS~X are all projects largely in~C.  Large applications, such as
compilers and word processors, on each system have been written in~C.

\section*{The Continual Growth of C++}

\index{programming!object-oriented}\index{programming!generic}Developed in the
1980s to support object-oriented programming, C++~was initially standardized
around 1998 with support for generic programming too.
\index{template}Allowing both the easy encapsulation of data behind an
interface and the template-driven generation of code, C++~enables even a lone
programmer effectively to develop and to maintain a large application.  The
complexity and subtlety of the language, however, make it not just powerful but
difficult to master in its entirety.

Beginning in 2011, and every three years since, the standard for C++~has
increased both the power and the complexity of the language.  New features
appear, and the occasional old feature is deprecated, both in the language and
in the standard library.  C++~feels like a moving target.  Yet the typical
C-program is still a valid C++-program.

With all of its exciting features, C++~is attractive to study, but mastering
all of C++~is a large project.
\index{ANSI}\index{header-file}\index{programming!generic}%
\index{template}\begin{itemize}
  \item There are 86 keywords in C++17 and 32~keywords in ANSI~C.
  \item The standard library for C++17 provides 82~header-files in comparison
    with C17's~29.
  \item Generic programming represents a vast new field of inquiry in its own
    right, including template-metaprogramming, all of which is entirely absent
    from C.
  \item {\it The C Programming Language} (K\&R) by \cite{KR1988} weighs in at
    272~pages, but {\it The C++ Programming Language} by \cite{S2013} weighs in
    at 1346~pages, and that does not include the material that would need to be
    added to describe C++14 and C++17 (let alone C++20!).
\end{itemize}

C++ is too large to cover in its entirety here.  In the chapters that follow,
we shall explore all of~C, but we shall explore only those features of
C++~pertinent to the examples and problems presented by K\&R.  That allows us
to focus on a small core of~C++.  This small core is tuned precisely to
represent the ideal starting-point for the programmer of~C.  So long as~C
remain a rough subset of~C++, the focused approach will allow for manageable
revisions to the present text, in order to keep it relevant even as
C++~continues to change and to grow.

\section*{Some Initial Comparisons}

\index{string}\index{array}As K\&R indicate in their introduction, ``C provides
no operations to deal directly with composite objects such as character
strings, sets, lists, or arrays.  There are no operations that manipulate an
entire array or string, although structures may be copied as a unit.''  Until
2011, this remained true of C++, so far as the language itself is concerned;
the standard library for C++ has always provided models for manipulating
strings, sets, lists, and arrays.  Such libraries have long existed for C too,
but C++ has always had \emph{standard} libraries for the composite objects, yet
the language itself did not know about them.  Since 2011, though, some support
for composite objects has crept into the C++ language.
\index{initializer_list@\texttt{initializer_list}}In certain contexts involving
a list surrounded by curly braces, the language now instantiates an
\texttt{initializer_list}, which is defined in the standard library.

The C language itself does not ``define any storage allocation facility other
than static definition and the stack
discipline provided by the local variables of functions; there is no heap or
garbage collection.''
\index{malloc@\texttt{malloc}!library-function}The standard library for C does
have a standard function \texttt{malloc} for the allocation of dynamic memory,
but the language knows nothing about that.
\index{new@\texttt{new}}\index{delete@\texttt{delete}}Since its initial
standard, however, C++ has always had the \texttt{new} and \texttt{delete}
keywords.
\index{free@\texttt{free}!library-function}By default \texttt{new} calls
\texttt{malloc} implicitly, and \texttt{delete} calls \texttt{free}.  So there
is some linguistic support for the dynamic allocation of memory in C++, whereas
there was none in C.

In their introduction, K\&R continue, ``C itself provides no input/output
facilities; there are no READ or WRITE statements, and no built-in file access
methods.''
\index{stdio.h@\texttt{stdio.h}}\index{iostream@\texttt{iostream}}This remains
true in C++, but the file-access methods originally in \texttt{stdio.h} have a
completely new interface in \texttt{iostream}.

``C offers only straightforward, single-thread control flow: tests, loops,
grouping, and subprograms, but not multiprogramming, parallel operations,
synchronization, or coroutines.''  Until 2020, this remained true of C++, so
far as the language itself is concerned.  Libraries for C and C++ have long
provided models for dealing with threads, and C++ since 2011 has support for
threads in the \emph{standard} library.  Since 2020, though, support for
coroutines has been added to the language via the keywords \texttt{co_await},
\texttt{co_yield}, and \texttt{co_return}.

To the extent that C++ tends to add functionality to the library---and not to
the language itself---, the language keeps to the spirit of C.  Even when the
language has incorporated a new keyword, such as \texttt{co_await}, the new
inclusion seems usually to follow a good principle: So much of the
functionality as possible is added to the library, and only so much as
absolutely necessary for the feature is brought into the language.  The
principle might not be followed in every case but seems usually to guide
change.

\section*{Organization of the Book}

The book roughly follows the outline of K\&R.  Before we begin that outline,
though, we shall go beyond K\&R to talk about tools that allow the reader to
follow along, regardless of hardware and operating system.

