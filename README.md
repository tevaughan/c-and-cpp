# C and C++

A C++-book written with a nod to Kernighan and Ritchie's classic, *The C
Programming Language*.

One should learn C before learning C++.  *C and C++* introduces all of C with
an eye toward modern C++.  The outline and content are similar to that of the
classic.  This is a full description of C and a thin (not comprehensive)
introduction to modern C++.

## Latest Version of This Document

- [HTML](https://tevaughan.gitlab.io/c-and-cpp)
- [PDF](https://tevaughan.gitlab.io/c-and-cpp/c-and-cpp.pdf)

