
\chapter{A Tutorial Introduction}

We consider first a broad overview of how to write some basic programs, each in
C and C++.  Many details will not be discussed, but the idea is to give working
examples so soon as possible.

\section{Getting Started}

The classic, first program just prints \texttt{hello, world}.  Before writing
this program, one should first become familiar with
\begin{itemize}[noitemsep]
  \item the shell
    and the
    commands\footnotpdfspace{%
      On a UNIX-like system the shell
      will probably be the Bourne-shell or the Bourne-again shell
      (BASH). The relevant commands will include \texttt{ls}, \texttt{mkdir},
      and \texttt{cd}.}
    for navigating the filesystem and
  \item a
    text-editor\footnotpdfspace{%
      A \emph{text-editor} (like \texttt{Notepad} on Micrososft's Windows or
      \texttt{nano} or \texttt{vi} on a UNIX-like system) is \emph{not} a
      word-processor.  A text-editor translates each press of a character on
      the keyboard into one byte in the text-file that the editor produces.  A
      word-processor, on the other hand, typically stores into the file not
      just the characters typed at the keyboard but also other bytes
      representing formatting, such as font-size, font-color, etc.}
    that can be invoked from the shell's command-line.
\end{itemize}

\subsection{Writing a Program}

A \emph{program} is a sequence of instructions for a computer to perform.  Each
of a C-program and a C++-program is defined by one or more \emph{text-files}
containing C-code or C++code.
\index{ASCII}A text-file is a file whose contents are, typically, only bytes
within the range of the character-set defined by the American Standard Code for
Information Interchange
(ASCII).\footnotpdfspace{See \url{https://asciitable.com}.  However, modern C
  and C++ compilers allow source-code to be encoded according to UTF-8, which
  is backward compatible with ASCII.  That is, an ASCII-file is a UTF-8 file,
  but not every UTF-8 file is an ASCII-file.  See also
  \ifpdf
  \url{http://unicode.org/faq/utf_bom.html\#UTF8}.
  \else
  \url{http://unicode.org/faq/utf_bom.html#UTF8}.
  \fi
  We shall assume in the text that every source-file is encoded in ASCII.}
A text-file containing C-code or C++-code typed by hand into a text-editor is a
\emph{source-file}, which contains \emph{source-code}.
Each source-file (and each code-file more generally) contains some combination
of
\index{declaration!type}\index{definition!type}\index{declaration!variable}%
\index{definition!variable}\index{declaration!function}%
\index{definition!function}\begin{itemize}[noitemsep]
  \item type-declarations,
  \item type-definitions,
  \item variable-declarations,
  \item variable-definitions,
  \item function-declarations, and
  \item function-definitions,
\end{itemize}
\index{header-file}along with one or more references to declarations and
definitions that exist in a
\emph{header-file}.\footnotpdfspace{%
  A header-file is (usually) a source-file that is referred to at the top, or
  \emph{head}, of another source-file.}
\index{main@\texttt{main}}The first program considered here (whether the
C-version or the C++-version) refers to a header-file and defines the function,
\texttt{main}.

\subsubsection{C}

Open a terminal to the shell-prompt, make a new directory if desired, change to
that directory, and edit a new file called \texttt{hello.c}.  Make it have the
following contents:
\VerbatimInput{examples/hello/hello.c}

\subsubsection{C++}

Also, edit a new file called \texttt{hello.cpp} with contents:
\VerbatimInput{examples/hello/hello.cpp}

\subsubsection{\texttt{\#include}}

\index{include@\texttt{\#include}}In each version of the program, there is a
line beginning with \texttt{\#include}.
\index{preprocessor}\index{argument!preprocessor-directive}Such a line is a
directive to the C \emph{preprocessor}.  The argument to the directive refers
to a file that the preprocessor will insert into the user's source-file, at the
point of the directive, and then the expanded file is given to the compiler.
In each of the two examples, the argument refers to a header-file in the
standard library.

\index{header-file}In C, each header of the standard library has a name ending
in~\texttt{.h}, but, in C++, no header of the standard library has
\texttt{.h}~at the end of its name.
\index{stdio.h@\texttt{stdio.h}}\index{iostream@\texttt{iostream}}%
\hrefpdffoot{https://en.cppreference.com/w/c/io}{\texttt{stdio.h}} gives the
compiler information about C's input/output library, and
\hrefpdffoot{https://en.cppreference.com/w/cpp/header/iostream}%
{\texttt{iostream}}
gives the compiler information about C++'s input/output library.

\subsubsection{\texttt{int main()}}

\index{main@\texttt{main}}Like C, C++ uses a function called \texttt{main} as
every program's entry-point.
\index{declaration!function}Both C++ and modern C (since C99) warn if no
\emph{return-type} for a function be specified in its declaration, though the
assumed return-type in that case is
\texttt{int}.\footnotpdfspace{In K\&R, no return-type was specified in the
  example corresponding to the the present program. The old behavior was
  silently to define the return-type as \texttt{int}.}
In C and C++, there are fundamental types built into the language, and there
are types that a user can define by way of the language, in terms of one or
more fundamental types.
\index{type!int@\texttt{int}}The type that \texttt{main} returns is the
fundamental type \texttt{int}, which corresponds to an integral value, which
can be positive, zero, or negative.  \texttt{main} ought explicitly to return
an integral value, which is what the shell expects to receive when the program
terminates.  Any value other than zero indicates an error to the shell.

\index{definition!function}\index{main@\texttt{main}}\index{declaration}%
\index{definition!variable}\index{statement}Inside the definition of any
function (like \texttt{main}) is a list of zero or more declarations,
definitions of variables, and \emph{statements}.
\index{function}A call to a function is an example of a statement.  Each of
\index{printf@\texttt{printf}}\index{return@\texttt{return}}%
\index{argument!return@\texttt{return}}\begin{itemize}[noitemsep]
  \item the call to \texttt{printf} and
  \item \texttt{return} together with its argument
\end{itemize}
is a statement.  The call to a function requires passing the information that
it expects in a parenthesized list of values. The \texttt{return}-statement
expects zero or one value (depending on the function's return-type), and
parentheses are not required.

\subsubsection{Printing the Message}

\index{printf@\texttt{printf}}\index{stdio.h@\texttt{stdio.h}}%
\index{standard output}\texttt{printf} is a function (declared in
\texttt{stdio.h}) that writes to the standard output.
\index{cout@\texttt{cout}}\index{endl@\texttt{endl}}%
\index{iostream@\texttt{iostream}}\index{namespace!std}In C++, a function or
variable, like \texttt{cout} or \texttt{endl} (declared in \texttt{iostream}),
might be declared inside of a \emph{namespace}, like \texttt{std}. C does not
have namespaces (at least not in the same sense).
\index{main@\texttt{main}}In a C++-program, the \texttt{main} function must be
defined outside of any namespace.
\index{operator!::@\texttt{::} scope}\index{UNIX!file-system}\index{namespace}A
function or variable inside a namespace can be accessed by way of the
scope-operator \texttt{::}, which works in analogy to the UNIX-like
file-system's directory-delimiter \texttt{/} in a
path.\footnotpdfspace{In this analogy, a namespace-name is like a
  directory-name; the last name on the right represents a function or variable,
  which is analogous to a filename.  Unlike an absolute pathname, which must
  begin with the delimiter (\texttt{/}), however, an absolutely qualified name
  for a variable, function, or type need not begin with scope-operator; rather,
  the leading namespace for an absolute name just needs to be a top-level
  namespace.  Otherwise, the compiler tries to resolve the symbol as a relative
  name.}

\index{declaration}\index{parameter}In C and C++, a function's
\emph{declaration} indicates the type returned by the function and lists the
type of each \emph{parameter} that the function accepts.
\index{argument!function}At the point where the function is called, the caller
passes \emph{arguments}, each of which is a named variable or expression, one
for each parameter in the function's declaration.

\index{function}\index{parameter}\index{declaration}In C, the name of a
function is associated with exactly one list of parameter-types; a function
with a given name can have exactly one declaration (though that same
declaration might appear in more than one place in the code).  For example, if
a C-program contain the declaration
\begin{Verbatim}
int sum(int a, int b);
\end{Verbatim}
then no different declaration for \texttt{sum} may exist in the program.
\index{printf@\texttt{printf}}\index{stdio.h@\texttt{stdio.h}}There is only one
declaration for \texttt{printf} in \texttt{stdio.h}.

\index{function}\index{parameter}In C++, however, the same name for a function
can be associated with more than one different list of parameter-types; a
function with a given name can be declared in more than one way.  For example,
a C++-program might contain the declarations
\begin{Verbatim}
int   sum(int   a, int   b);
float sum(float a, float b);
\end{Verbatim}
\index{overloading}\index{declaration}\index{argument!function}This is known as
\emph{overloading}.  Which declaration is used depends on the type of each
argument in the list at the calling site.
\index{operator}Overloading is possible not just for an ordinary function but
also for an operator.

\index{operator!<<@\texttt{<<}}\index{iostream@\texttt{iostream}}The operator
\texttt{<<} is declared in many different ways in \texttt{iostream}.
\index{declaration}Yet in every declaration, the operator's return-value and
left-operand are of the same type:
\index{reference}\index{ostream@\texttt{ostream}}a \emph{reference} to type
\texttt{ostream}.  What differs from declaration to declaration is the type of
the right-operand.

\index{iostream@\texttt{iostream}}\index{operator!<<@\texttt{<<}}For
\texttt{hello.cpp} to work, we need two different declarations in
\texttt{iostream} for the \texttt{<<} operator:
\index{endl@\texttt{endl}}\begin{enumerate}[noitemsep]
  \item One with a right-operand of \texttt{"hello, world"}'s type.
  \item Another with a right-operand of \texttt{endl}'s type.
\end{enumerate}
In each case, what is on the right, regardless of its type, is in some sense
inserted into the output-stream.
\index{endl@\texttt{endl}}The \texttt{endl} inserts a new-line character (see
below) and causes the output-stream to flush its
buffer.\footnotpdfspace{By default, the standard output
  is buffered.  So using \texttt{printf} or \texttt{<<} to send a single
  character might not immediately produce any visible output.  In C, the
  standard output is flushed---so that any buffered characters will be
  displayed---by way of the function \texttt{fflush} in the standard library.
  All output-buffers are flushed when a program exits back to the shell.  So
  explicitly flushing the output-buffer is not required for a short program.
  But if a program print some characters just before spending a long time on a
  calculation, then those characters might be visible before the calculation is
  finished only if the buffer were flushed before beginning the calculation.}
\index{cout@\texttt{cout}}\index{ostream@\texttt{ostream}}The variable
\texttt{cout} is of type \texttt{ostream} and represents the standard output.
The \texttt{<<} operator returns a reference to the same instance of
\texttt{ostream} that was passed in as its left operand.

\index{character!"@\texttt{"} quote}\index{string}In the source-code for a C or
C++ program, a sequence of characters beginning and ending with double quotes,
like \texttt{"hello, world\textbackslash n"} or \texttt{"hello, world"}, stands
for a \emph{character string}.
\index{string!literal}In fact, such a sequence stands for a special kind of
character string, a \emph{string-literal}, whose entire value is known to the
compiler.  The double-quote character~\texttt{"} is part of the
string-literal's representation in the source-code, but the double-quote
character is not part of the string that the literal represents. Rather, the
double-quote character delimits the characters that in the source-code stand
for characters in the string.  In general, a string in C or C++ might (e.g., by
interaction with the user) be constructed while the program is running, so that
compiler cannot know the value.  But a string-literal is a string that is
defined in the source-code and thus known to the compiler.

\index{character!\texttt{\textbackslash\textbackslash} back-slash}%
\index{character!n@\texttt{\textbackslash n} new-line}The
\texttt{\textbackslash n} stands for a single character, the \emph{new-line
character}, though it is represented by two characters (\texttt{\textbackslash}
and \texttt{n}) in the source-code.
\index{string!literal}In the C-program, it was at the end of the
string-literal.
\index{endl@\texttt{endl}}In the C++-program, it does not appear, but the
new-line character is inserted at the end of the printed string by
\texttt{endl}.  When printed, the new-line character advances the output to the
left margin and down to the next
line.\footnotpdfspace{Unless the program be compiled by a compiler from
  Microsoft.  In that case, \texttt{\textbackslash n} merely advances the
  output down to the next line but without returning to the left margin.}

\index{character!n@\texttt{\textbackslash n} new-line}%
\index{escape-sequence}The two characters \texttt{\textbackslash n}
representing the new-line character provide an example of an
\emph{escape-sequence}.  An escape-sequence provides a mechanism for
representing hard-to-type or invisible characters.
\index{character!t@\texttt{\textbackslash t} tab}%
\index{character!b@\texttt{\textbackslash b} back-space}Among other available
escape-sequences are \texttt{\textbackslash t} to the tab-character and
\texttt{\textbackslash b} for the back-space character.  The escape-sequence
for the new-line character is required in order to force a new line in the
output.
\index{character!"@\texttt{"} quote}Beginning a string-literal on one line by
typing \texttt{"} into the source-code, embedding a new-line character by
typing Enter into the source-code, and then ending the string-literal on the
next line by typing \texttt{"} into the source-code will produce an error when
the compiler is run.  That is,
\begin{Verbatim}
printf("hello, world
");
\end{Verbatim}
will not work.

\subsubsection{\texttt{return 0}}

\index{main@\texttt{main}}\index{type!void@\texttt{void}}Because \texttt{main}
has a non-\texttt{void} return-type, we provide a return-value.  A value of
zero indicates to the shell that the program ended normally.

\subsection{Compiling the Source}

\index{compiling}After saving each file and returning to the shell, type the
following (hit Enter after each line):
\begin{Verbatim}
cc  -o hello-c   hello.c
c++ -o hello-cpp hello.cpp
\end{Verbatim}
The standard name for the C++-compiler is \texttt{c++} (and the standard name
for the C-compiler is still \texttt{cc} as indicated in K\&R).  The name of a
source-file for C++ typically ends in \texttt{.cpp}, but the compiler will
likely accept \texttt{.cxx}, \texttt{.c++}, \texttt{.cc}, and others.

\index{a.out@\texttt{a.out}}By default, if no name for the executable
output-file be given, then
\texttt{a.out}\footnotpdfspace{A fascinating account of why the default name
  for the executable program is \texttt{a.out} is given by
  \cite{R1993a,R1993b}.}
is still used, as indicated in K\&R for the C-compiler.  In the example here,
however, we have used, \texttt{-o hello-cpp} on the command-line in order to
specify the name (\texttt{hello-cpp}) of the executable output-file emitted by
the C++-compiler.

\subsection{Running the New Program}

Now that the source has been compiled into an executable, type the following
commands at the shell-prompt:
\begin{Verbatim}
./hello-c
./hello-cpp
\end{Verbatim}
This example of how to invoke a new program from the shell is different from
the invocation in K\&R and reflects a change that has happened over the years,
at least in the typical UNIX-like environment.  Since the mid-1990s or so, as
it seems to me, the standard has been not to include the current directory in
the list of directory-paths that the shell uses to match an executable's
filename against a command.

This approach adds a measure of security.  If somehow an executable binary or
shell-script, sitting in some directory, happened to have the same name as a
standard command, then the user, after navigating to that directory at the
shell-prompt, might accidentally invoke that binary or shell-script when
intending to launch the standard command.  Not including the current working
directory in the default list of directory-paths requires that an executable in
the current directory be launchable only if the command be invoked by an
absolute path or a path explicitly relative to the current directory.

For each of the commands just entered above, the output should look like this:

\VerbatimInput{examples/hello/hello-cpp.out}
That output should be the same for both programs.

\begin{exercise}
  Verify that the same code in \texttt{hello.c}, if it reside instead in a file
  whose name ends in \texttt{.cpp}, will compile via \texttt{c++} to produce an
  executable and that the program produces identical output.
\end{exercise}

\index{ASCII}\begin{exercise}
  Write a C-program that determines, for every ASCII non-whitespace character
  between \texttt{!} (decimal-integer-value 33) and \texttt{\texttildelow}
  (126), inclusive, whether that character have a special meaning when prefixed
  by \texttt{\textbackslash} and appearing in a string delimited by double
  quotes.\footnotpdfspace{See \url{http://www.asciitable.com/}. Also note that
    if \texttt{"} be printable, then \texttt{\textbackslash "} must have a
    special meaning even if it do not stand for something other than
    \texttt{"}. That is, \texttt{\textbackslash "} is special in that it
    prevents \texttt{"} from delimiting a string and instead causes \texttt{"}
    to appear in the output.}
\end{exercise}

\begin{exercise}
  Verify that the C-program in the previous exercise compiles and runs properly
  when it resides in a file whose name ends in \texttt{.cpp} and when it is
  compiled by \texttt{c++}.
\end{exercise}

\begin{figure}
  \begin{small}
    \index{printf@\texttt{printf}}\index{loop!while@\texttt{while}}%
    \VerbatimInput{examples/temperature/temperature.c}
  \end{small}
  \caption{C-code to print table of conversions from Fahrenheit to Celsius.}
  \label{fig:fc-table-c}
\end{figure}
\begin{figure}
  \begin{small}
    \index{cout@\texttt{cout}}\index{endl@\texttt{endl}}%
    \index{loop!while@\texttt{while}}%
    \VerbatimInput{examples/temperature/temperature.cpp}
  \end{small}
  \caption{C++-code to print table of conversions from Fahrenheit to Celsius.}
  \label{fig:fc-table-cpp}
\end{figure}
\begin{figure}
  \begin{small}
    \VerbatimInput[obeytabs=true]{examples/temperature/c-cpp.diff}
  \end{small}
  \caption{Difference between C-code and C++-code for printing table of
  conversions from Fahrenheit to Celsius. Difference is generated via
  \texttt{diff -u}.}
  \label{fig:fc-diff}
\end{figure}
\begin{table}
  \begin{small}
  \VerbatimInput[obeytabs=true]{examples/temperature/temperature-c.out}
  \end{small}
  \caption{Conversions from Fahrenheit to Celsius.
  \ifpdf
  \else
  (In the table displayed above, the tab-character between the columns might
  not be rendered properly.)
  \fi
  }
  \label{tab:fc}
\end{table}

\section{Variables and Arithmetic Expressions}

Each of the C-program in Figure~\ref{fig:fc-table-c} and the C++-program in
Figure~\ref{fig:fc-table-cpp} uses
\begin{equation}
  C = \frac{5}{9}[F - 32],
\end{equation}
where $C$ represents the number of degrees Celsius, and $F$ represents the
number of degrees Fahrenheit, in order to print Table~\ref{tab:fc}.  The
difference between the C-code and the C++-code is listed in
Figure~\ref{fig:fc-diff}.

Like the code for the hello-program, the code for the program that prints the
table defines a single function, \texttt{main}.
\index{comment}\index{declaration!variable}\index{variable}%
\index{expression!arithmetic}\index{loop}\index{formatted output}Just as K\&R
remark for their corresponding example, the new code ``introduces several new
ideas, including comments, declarations, variables, arithmetic expressions,
loops, and formatted output.''

\subsection{Comments}

\index{comment}The first thing to note is that there are \emph{comments} in the
code.  A comment either
\begin{itemize}
  \item (according to the newer style) begins with \texttt{//} and persists
    until the end of the line or
  \item (according to the original style) begins with \texttt{/*} and persists
    until the next \texttt{*/}, regardless of whether the \texttt{*/} lie on
    the same line or on a later line.
\end{itemize}
Everything in a comment is ignored by the compiler.  The C-code in
Figure~\ref{fig:fc-table-c} has a few changes from the corresponding example
K\&R. One of the changes has to do with comments. Since C99, a comment
beginning with \texttt{//} and persisting until the end of the line is
permitted; in the C89 of K\&R, such a comment was not part of the standard.
C++ was first standardized around the same time as C99, and, since that time,
each of C and C++ has allowed both kinds of comment.  A comment is allowed,
outside of a string-literal, wherever a space, a tab, or a newline is allowed
in the code. Good practice for the use of comments includes
\begin{itemize}
  \item indicating, on the include-line for a header-file, what symbol declared
    in the header file is used in the current source-file,
  \item summarizing what a function does,
  \item describing the meaning of each variable when it is declared, and
  \item briefly indicating what a statement does.
\end{itemize}
In the figures, the last line of each code-file is a comment that would be used
by the \texttt{vim} text-editor, if it be used to edit the file.  In the
PDF-version of the book, the maximum number of characters that will fit on a
line, when the code is displayed as a figure, is~48; the \texttt{tw=48} sets
the editor's \texttt{textwidth}-option to have the value,~48.  This setting
allows the editor automatically to wrap the line at the appropriate column.

\subsection{Variables and Their Types}

\index{declaration!variable}\index{variable}\index{type}In C and C++, a
\emph{variable} must be \emph{declared} before it can be used.  The declaration
announces the variable's \emph{type}.  The declaration can also specify an
initial value for the variable. A declaration can announce a list of variables,
or just a single variable.
\index{int@\texttt{int}}For example, in
\begin{Verbatim}
int f_lo= 0, f_hi= 300, df= 20;
\end{Verbatim}
the variables \texttt{f_lo}, \texttt{f_hi}, and \texttt{df} are declared to be
of type \texttt{int}, which indicates an integer, and the initial value for
each is indicated after an equal-sign.
\index{statement}\index{assignment}In a \emph{statement},
\begin{itemize}[noitemsep]
  \item a function is called,
  \item an \emph{arithmetic calculation} is performed, and/or
  \item an \emph{assignment} is made to a variable.
\end{itemize}
As for a statement, a \emph{declaration} (whether it have an initializer or
not) is terminated by a semicolon.
\index{float\texttt{float}}For example, the declaration
\begin{Verbatim}
float a;
\end{Verbatim}
with no initializer declares the variable \texttt{a} to be of type
\texttt{float}, but it has no specified initial value.  For variables, Each of
the C and the C++ languages provides a few fundamental, built-in types:
\index{char@\texttt{char}}\index{short@\texttt{short}}%
\index{int@\texttt{int}}\index{long@\texttt{long}}%
\index{long long@\texttt{long long}}\index{double@\texttt{double}}%
\index{long double@\texttt{long double}}\begin{itemize}[noitemsep]
  \item \texttt{char}: character (a single byte),
  \item \texttt{short}: integer of at least two
    bytes,
  \item \texttt{int}: integer of at least two bytes,
  \item \texttt{long}: integer of at least four bytes,
  \item \texttt{long long}:\footnote{%
      Type \texttt{long long} is part of the C-standard since C99 and part of
      the C++-standard since C++11.}
    integer of at least eight bytes,
  \item \texttt{float}: single-precision (four-byte) floating-point number,
  \item \texttt{double}: double-precision (eight-byte) floating-point number.
  \item \texttt{long double}: extended-precision floating-point number.
\end{itemize}
There are some fundamental types found only in C (since C99):
\index{_Bool@\texttt{_Bool}}\index{_Imaginary@\texttt{_Imaginary}}%
\index{_Complex@\texttt{_Complex}}\begin{itemize}[noitemsep]
  \item \texttt{_Bool}:\footnote{%
      Type \texttt{_Bool} since C99 is similar to type \texttt{bool} in C++.
      Type \texttt{bool} since C99 is not a fundamental type but is defined in
      the standard library as a macro of the C-preprocessor and stands for
      \texttt{_Bool}, which is what the compiler sees.  Converting any nonzero
      integer or floating-point value (such as~0.1) to \texttt{_Bool}
      produces~1.}
    logical~1 or~0,
  \item \texttt{float _Imaginary}: imaginary number,
  \item \texttt{double _Imaginary}: imaginary number,
  \item \texttt{long double _Imaginary}: imaginary number,
  \item \texttt{float _Complex}: complex number,
  \item \texttt{double _Complex}: complex number,
  \item \texttt{long double _Complex}: complex number.
\end{itemize}
There are some fundamental types found only in C++.
\index{bool@\texttt{bool}}\index{wchar_t@\texttt{wchar_t}}%
\index{char8_t@\texttt{char8_t}}\index{char16_t@\texttt{char16_t}}%
\index{char32_t@\texttt{char32_t}}\begin{itemize}[noitemsep]
  \item \texttt{bool}:\footnote{%
      Type \texttt{bool} in C++ is similar to type \texttt{_Bool} since~C99.
      Converting any nonzero integer or floating-point value (such as~0.1) to
      \texttt{bool} produces \texttt{true}.}
    logical \texttt{true} or \texttt{false},
  \item \texttt{wchar_t}:\footnote{%
      Type \texttt{wchar_t} is a fundamental type in~C++, but it is available
      in~C (since C99) as part of the standard library. The size is four bytes
      on a system that supports Unicode, except on MS Windows, where support is
      limited to UTF-16.}
    character-type for any code-point,
  \item \texttt{char8_t}:\footnote{Since C++20.} character-type for UTF-8,
  \item \texttt{char16_t}:\footnote{Since C++11.} character-type for UTF-16,
  \item \texttt{char32_t}:\footnote{Since C++11.} character-type for UTF-32.
\end{itemize}
\index{array}\index{structure}\index{union}\index{pointer}\index{function}There
are also \emph{arrays}, \emph{structures}, and \emph{unions} of these
fundamental types, \emph{pointers} to any of the above, and \emph{functions}
that can accept and return any of the above.

\subsection{Looping}

\index{loop!while@\texttt{while}}\index{while@\texttt{while}}In each of the
C~and~C++ programs, computation takes place in a loop introduced by
\texttt{while}.
\begin{Verbatim}
  while(f <= f_hi) {
    ...
  }
\end{Verbatim}
Each line of the table requires the same calculation, and so each line is
printed during a pass through the loop.  \index{condition}When the program's
execution reaches the line containing the \texttt{while}-keyword, the
\emph{condition} in parentheses is evaluated. If it be true, then the
statements in the loop are executed. This is repeated until the condition be
false, when the statements in the loop are skipped, and execution continues
just past the end of the loop.  At that point, there is one remaining statement
in the program; this statement returns the value~0 to the shell as the program
exits.

The body of a \texttt{while}-loop, can be either a single statement or,
surrounded by braces, a collection of zero or more statements.  For example,
suppose that there is a function \texttt{foo} that takes no arguments, that
returns a value of type \texttt{int}, and that at some point it will return the
value~0. Then
\begin{Verbatim}
  while(foo())
    ;
\end{Verbatim}
will repeatedly both call \texttt{foo} and then, if \texttt{foo} return a
nonzero value, execute the empty-statement (represented by \texttt{;}).  If
\texttt{foo} ever return a nonzero value, then the loop will exit.

