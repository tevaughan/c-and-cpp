
\chapter{Preface}

This book offers a complete study of~C and an encounter with C++. The~C
programming language is relatively simple.  C++~is relatively complex. Yet
almost every C-program is a valid C++-program.  In the chapters below, we use
the study of~C as a lens through which to focus on the basic structure of~C++
and on some of its features.

C~and its standard library are now essentially the same as they were in 1990
(despite a few refinements), but C++~is redefined by a new standard every three
years.  With each new standard, C++~adds features, and the occasional old
feature is deprecated.  The disparity between the complexity of~C and that
of~C++ seems likely to grow for the foreseeable future.  Mastering C++~was
already a daunting prospect before the standard began its regular evolution in
2011.  Mastery might seem all the more daunting---even for one who achieved
some level of mastery in the past!---because the language and the library
continue to change.

\section*{The Aim of the Book}

I propose an introductory approach to C++:\\
\\
\emph{For every example in a study of~C, consider how the example would best be
written in~C++.}\\
\\
This eliminates most of~C++ from discussion.  The approach focuses on aspects
of~C++ relevant to fundamental problems encountered in studying~C.

The proper way to study C~is by way of \cite{KR1988} (K\&R).  So my outline is
like theirs.

\section*{The Persistent Value of C}

The landscape of computing is changed substantially since 1988.  Yet learning~C
remains valuable because of
\begin{itemize}[noitemsep]
  \item the good features of the language,
  \item its stability, and
  \item the installed base of code needing maintenance.
\end{itemize}
Over time, languages rise and fall in popularity among programmers. C~seems to
have remained among the most popular languages since the 1980s, when C~grew
toward
standardization.\footnotpdfspace{Although the text of 1988 was the second
  edition of the Kernighan and Ritchie's book, it corresponds to the first
  C-language standard promulgated by a standards-organization (ANSI).}

The~C programming language has many good features. Here are some of them:
\begin{enumerate}[noitemsep]
  \item C is small.
  \item C is efficient.
  \item C and its standard library are complete.
  \item Using C is good for the programmer.
\end{enumerate}
As to the first, C~is easy to learn in its entirety.  Second, a C-program, even
if it be large, can both compile and execute faster than the equivalent program
in almost any other language.  Third, the language and its library provide
everything needed to write any kind of program.  Fourth, mastering C~gives
intuition about how computing machinery works at a deep level, intuition about
how to write efficient code even in other languages.

\index{ANSI}\index{IEC}\index{ISO}The stability of C---whose original standard,
C89\footnotpdfspace{ANSI X3.159-1989}, was promulgated by the American National
Standards Institute (ANSI)---is reflected by the small difference between the
original standard and the current standard, C18\footnotpdfspace{ISO/IEC
9899:2018}---promulgated by jointly by the International Standards Organization
(ISO) and the International Electrotechnical Commission (IEC)---.  There are
some differences, to be sure, and we shall note them where appropriate, but the
basic features of the language are as they were. Thus both the outline and the
content of K\&R remain valuable for instruction.  As history unfolds, C seems
like a bedrock upon which C++ is built.  C++ continues to grow and to change,
but it retains deep interoperability with C, which remains mostly unchanged.

A large corpus of code is written in C.
\index{kernel}The
kernel\footnote{%
  A \emph{kernel} is a special program that begins to run before any other
  program in the operating system begins.  The kernel does not end its run even
  as other programs begin running and eventually come to an end; when the
  kernel ends, the system as a whole stops. The kernel interacts directly with
  hardware in the system, and other programs usually interact with hardware
  only by communicating with the kernel. The kernel in a time-sharing operating
  system, like UNIX, may pause the execution of any other program in the system
  and then allow that program to resume its run.}
of an operating system is typically written in~C, and one can find an example
of almost every kind of user-space (non-kernel) application, including the
interpreter for many a scripted language, that is written in~C.
\index{UNIX}The C~programming language originated in the development of the
UNIX operating system.
\index{UNIX!kernel}The UNIX kernel was translated from assembly to C in 1973.
Between 1973 and 1978, the language gained most of its modern features, and the
user-space programs for UNIX were translated to C \citep{R1993a,R1993b}.
\index{kernel!Linux}Introduced in the early 1990s, the
\hrefpdffoot{https://kernel.org}{Linux kernel} enables a
\hrefpdffoot{https://opengroup.org/membership/forums/platform/unix}{UNIX-like}
operating system whose source-code is freely redistributable.  The Linux kernel
is one of the most widely used operating-system kernels ever to be produced.
It has been written exclusively in C for almost all of its history. The origin
of~C lies in the development of UNIX, and C~has continued to play a central
role in the development of UNIX-like systems based on the Linux kernel and on
other UNIX-like kernels, but C has been adapted to serve in the development
Microsoft's Windows and other operating systems too. From operating-system
kernels to large user-facing applications, programs under development in~C will
likely remain widespread for the foreseeable future.

\section*{What This Book Is}

K\&R's outline and scope are about the same as those of the present work, but I
discuss modern C, and I discuss C++ too. The examples from K\&R are updated,
and examples for C++ are added.  Wherever appropriate, each of a modern-C
version and a C++-version is provided as an example in the text or as a
solution to a classic problem.  So the reader can make a direct comparison
between C and C++.  I choose C++17\footnotpdfspace{ISO/IEC 14882:2017} for the
discussion of C++ because I have years of practical experience with C++17, but
not yet with C++20.  If this project prove useful, the next version of the book
will address a future standard.

One needs neither to have a copy of K\&R handy nor to have read K\&R.  The
present book is self-contained, but I do make frequent reference to K\&R, so
that one who has a copy might find the comparison useful.

The book is available in various forms:
\begin{itemize}[noitemsep]
  \item \hrefpdffoot{https://gitlab.com/tevaughan/c-and-cpp}{source-code},
  \item \hrefpdffoot{https://tevaughan.gitlab.io/c-and-cpp}{HTML}, and
  \item \hrefpdffoot{https://tevaughan.gitlab.io/c-and-cpp/c-and-cpp.pdf}{PDF}.
\end{itemize}

\section*{What This Book Is Not}

{\it\doctitle} is the opposite of a comprehensive guide to C++.  The book
rather provides a mere introduction, tailored to the student of C.  Even with
regard to C, the book is incomplete in that it does not describe in detail the
standard for C as K\&R do.

The reader who wishes to explore the full depth of modern C++ should refer to
\begin{itemize}
  \item {\it The C++ Programming Language} \citep{S2013}, which covers {C++11}
    in detail,
  \item {\it Effective Modern C++} \citep{M2014}, which covers the biggest
    changes in going from C++98 to C++11 and to C++14, and
  \item {\it C++: The Complete Guide} \citep{J2019} which covers all that is
    new in C++17.
\end{itemize}
A detailed technical reference for every standard of both C and C++ is
available at \url{https://cppreference.com}.

\vspace{0.25in}

\begin{flushright}
Thomas E. Vaughan\\
2020 August
\end{flushright}

