
# $(ROOT)-c-cpp is same code as $(ROOT)-c but compiled by c++.
PROGRAMS:=$(ROOT)-c $(ROOT)-c-cpp $(ROOT)-cpp

WARNINGS:=$(PROGRAMS:=.warn)
OUTPUTS:=$(PROGRAMS:=.out)

WARNFLAGS:=-W -Wall -Wextra -Wpedantic
CXXFLAGS:=$(WARNFLAGS) -std=c++17
CFLAGS:=$(WARNFLAGS) -std=c17

%.out: %
	@if ./$< >$@; then\
	  echo $< exited normally;\
	else\
	  echo $< exited with code $$?;\
	fi

%-cpp: %.cpp
	@if $(CXX) $(CXXFLAGS) -o $@ $< > $@.warn 2>&1; then\
	  echo "building $@";\
	else\
	  echo "ERROR: failure to build $@:";\
	  cat $@.warn;\
	fi

%-c: %.c
	@if $(CC) $(CFLAGS) -o $@ $< > $@.warn 2>&1; then\
	  echo "building $@";\
	else\
	  echo "ERROR: failure to build $@:";\
	  cat $@.warn;\
	fi

.PHONY: all clean

all: $(PROGRAMS) $(OUTPUTS) c-cpp.diff
	@if diff $(ROOT)-c.out $(ROOT)-cpp.out; then\
	  echo "identical '$(ROOT)-c.out' and '$(ROOT)-cpp.out'";\
	else\
	  echo "ERROR: '$(ROOT)-c.out' and '$(ROOT)-cpp.out' differ";\
	fi

$(ROOT)-c.cpp:
	@ln -fsv $(ROOT).c $@

c-cpp.diff: Makefile
	diff -u $(ROOT).c $(ROOT).cpp\
	  |grep -v $(ROOT).c\
	  |grep -v $(ROOT).cpp>$@;\
	true

clean:
	@rm -fv $(PROGRAMS) $(WARNINGS) $(OUTPUTS) $(ROOT)-c.cpp c-cpp.diff

