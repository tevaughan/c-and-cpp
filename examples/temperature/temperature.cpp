#include <iostream> // cout, endl
// Print Fahrenheit-Celsius table for
//   f = 0, 20, ..., 300,
// where f is the number of degrees Fahrenheit.
int main() {
  // low-limit, high-limit, and step-size for
  // table, all in units of Fahrenheit (F)
  int f_lo= 0, f_hi= 300, df= 20;
  int f= f_lo; // current temperature (F)
  while(f <= f_hi) {
    // current temperature (C)
    int c= 5 * (f - 32) / 9;
    std::cout << f << /* tab */ "\t" << c
              << /* flush, newline */ std::endl;
    f= f + df; // next temperature (F)
  }
  return 0;
}
// vim: set tw=48:
