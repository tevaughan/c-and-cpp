
\chapter{Tools}

We begin with a quick introduction to the most basic tools for writing software
in C and C++ by way of a \emph{command-line}.  Since the publication of {\it
The C Programming Language} (K\&R), tools for writing C and C++ programs have
become widely available as open-source software.%
\footnote{%
  Whether software be \emph{open-source} or \emph{free} arguably depends,
  according to usage popularized by the Open Source Initiative (OSI,
  \url{https:://opensource.org}) and the The Free Software Foundation (FSF,
  \url{https://fsf.org}), on the license used to protect the software's
  source-code under copyright-law.  If the source-code be open or free, then it
  is likely that one can find a no-cost way to download an executable image of
  the software for one's computer.%
}
Anyone who has access to the internet should be able to install command-line
tools that will allow one to follow along with the narrative below.

\section*{Command-Line}

In the 1970s, when the earliest C-programs were written, the author of a
program worked at a \emph{terminal}.%
\footnotpdfspace{%
  Perhaps the device was called a ``terminal'' because it sat at the terminus
  of a cable running from a serial-port on the computer.
}
The terminal had a keyboard for input and, for output, either a printer with
long, continuous sheet of paper or a cathode-ray tube for a dynamic text-display.
By 1975, Digital Equipment Corporation was selling the VT-52 terminal, which
supported $80 \times 24$ (upper-case and lower-case) characters, with enough
buffer for limited scroll-back.  See Figure~\ref{fig:vt52}.

\begin{figure}
  \begin{center}
    \href{https://tevaughan.gitlab.io/c-and-cpp/html/VT52-3.jpg}{
      \ifpdf
      \includegraphics[width=0.9\columnwidth]{VT52-3.jpg}
      \else
      \includegraphics[width=0.9\columnwidth,natwidth=2240,natheight=1680]{VT52-3.jpg}
      \fi
    }
    \caption{VT-52 terminal. $80 \times 24$ characters, upper and lower case.
    Released around 1975.}
    \label{fig:vt52}
  \end{center}
\end{figure}

The computer attached to the terminal ran a program called a \emph{shell}.  The
shell printed a command-prompt and waited for input from the keyboard.
\index{argument!command}In response to the prompt, on a single line (the
command-line), the user typed a command-word and possibly other words, each an
\emph{argument} to the command.  On receipt of a valid command and arguments,
the shell transferred the terminal's input and output to the program indicated
by the command.  When the program exited, the terminal's input and output
returned to the shell, which printed another command-prompt and again waited
for input.

From the command-line, the user could launch an editor to write source-code,
launch a compiler (and/or linker) to generate an executable program from the
source-code, or even launch the new executable program.  For the most basic
inputs to and output from a new program, the C programming language and its
standard library allow the author of a program to have that program
\index{argument!command}\index{standard input}\index{standard output}%
\begin{itemize}
  \item obtain any arguments that were on the command-line after the program's
    name,
  \item read from the \emph{standard input}, which ordinarily comes from the
    terminal's keyboard when the program is running, and
  \item write to the \emph{standard output}, which ordinarily goes to the
    terminal's display when the program is running.
\end{itemize}
C++ provides the same basic inputs and output.
\index{argument!command}\index{standard input}\index{standard output}Most of
the examples in this book will use as inputs only the command-line arguments
and the standard input, and most examples will use for output only the standard
output.

In order to write basic software in C++, then, one needs some basic tools. In
the first place, one needs a \emph{terminal-emulator} (or an actual terminal!).
One needs also an operating system that connects a shell
to the terminal.  Finally, the shell
must provide the ability to launch any of
\index{compiling}\begin{itemize}
  \item an editor,
  \item commands for navigating and manipulating the filesystem in which edited
    source-code files are stored and retrieved,
  \item the compiler, and
  \item an executable program emitted by the compiler.
\end{itemize}
If one already have access to a terminal, a command-shell, and basic tools,
then one need not install them, but there are some hints below about how to
install an appropriate environment.

\section*{Installation Under Microsoft Windows}

\index{compiling}In principle, even Microsoft's old DOS shell or more modern,
native counterparts can provide the required environment via native tools for
editing, compiling, and navigating the filesystem.
\index{UNIX}However, installing a UNIX-like environment is arguably the easiest
approach.

\index{UNIX}There are several different ways to install a UNIX-like environment
under Microsoft Windows.  The main options boil down to using
\hrefpdffoot{https://cygwin.com}{Cygwin}, using
\hrefpdffoot{https://docs.microsoft.com/en-us/windows/wsl/install-win10}{WSL},
or using a virtual machine.

\subsection*{Cygwin}

If one's computer have as its operating system just about any version of
Microsoft Windows since Windows XP, then one can install Cygwin.  Install the
32-bit version only if absolutely necessary.  Some details about Cygwin are
provided here because it seems to represent the easiest way to obtain an
appropriate environment under Microsoft Windows.

When running the installer, it is safe to choose the defaults provided, but one
must explicitly select a download-site from a list of suggestions.  In the
Select Packages window, change the View to `Category.'  Click to open the Devel
category.
\index{compiling}Install the \texttt{gcc-g++} package (for the compilers) by
finding its line and double-clicking on the `Skip' field.  Further down in the
list, do the same thing for the \texttt{make} package.  Then click to open the
Editors category.  As with the packages above install a terminal-based
text-editor, like \texttt{emacs} or \texttt{vim}, if a familiar editor be
listed; otherwise, install \texttt{nano}.
\hrefpdffoot%
{https://en.wikibooks.org/wiki/Guide_to_Unix/Commands/File_Editing}%
{Documentation} on how to use \texttt{nano} is easy to find.

Unless one want to install X-windows (not described here), do not to pick an
editor that requires a graphical environment.  One could avoid installation
of an editor here and instead just use Notepad or another native-Windows
text-editor, but then one will need to navigate to one's home-directory
under Cygwin's directory-tree in order to edit files.

Click the Continue button at the bottom of each window until the installation
is done.  Finally, launch Cygwin's Terminal to get a shell-prompt.

\subsection*{WSL}

If one have a recent enough version of Windows 10, one may install WSL and pick
one's favorite distribution of Linux from the Microsoft Store.

\subsection*{Virtual Machine}

\index{UNIX}One might install virtualization-software, like
\hrefpdffoot{https://www.virtualbox.org/}{Oracle's VirtualBox}, and then
install one's favorite distribution of a UNIX-like operating system into a
virtual machine.

\section*{Installation Under MacOS}

\index{compiling}The easiest approach here is to install
\hrefpdffoot{https://developer.apple.com/xcode/}{Xcode}.  After installing
Xcode, the C and C++ compilers should be available in the shell
that runs in the Terminal app.

\section*{Installation on Bare Metal}

Each distribution of an operating system has its own instruction about how to
install it as the primary operating system on a computer.  Any distribution of
Linux or BSD will have all of the tools needed for writing programs in C and
C++.
