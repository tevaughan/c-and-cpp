DOCNAME = c-and-cpp
TEXNAME = $(DOCNAME).tex
PDFNAME = $(DOCNAME).pdf

# You want latexmk to *always* run, because make does not have all the info.
# Also, include non-file targets in .PHONY so they are run regardless of any
# file of the given name existing.
.PHONY : c-and-cpp.pdf all clean

all : $(PDFNAME) html

# --shell-escape is needed for gnuplottex

$(PDFNAME) : *.tex avatar-icon.png *.bib
	(cd examples; make)
	latexmk -pdf -use-make $(TEXNAME)

# Both
# (A) second and third arguments to 'htlatex' and
# (B) contents of 'nma.cfg'
# were obtained from 'https://www.12000.org/my_notes/faq/LATEX/htse54.htm'.
html : $(PDFNAME) *.bib
	(cd examples; make)
	@mkdir -p html
	@rm -frv html/*
	@cp -rv *.tex *.mk4 *.bib nma.cfg *.png *.jpg examples html
	# Don't know yet how to turn on --shell-escape with make4ht.
	# -u produces UTF-8 encoding, needed for \textregistered.
	# $(DOCNAME).mk4 exists, and so that will be used as build filename.
	@(cd html; make4ht -u -e $(DOCNAME).mk4 $(TEXNAME) "fn-in")

clean :
	(cd examples; make clean)
	@rm -frv html
	@rm -fv *.bbl
	latexmk -CA

